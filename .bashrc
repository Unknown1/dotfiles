#!/usr/bin/env bash
stty -ixon # Disable ctrl-s and ctrl-q.
shopt -s autocd #Allows you to cd into directory merely by typing the directory name.
HISTSIZE= HISTFILESIZE= # Infinite history.
export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"

[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc" # Load shortcut aliases
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"
neofetch
set -o vi
alias I='sudo apt install'
alias U='sudo apt update && sudo apt upgrade'
alias v='nvim'
alias S='sudo apt search'
alias R='sudo apt remove'
alias aR='sudo apt autoremove'
alias u='unimatrix -s 95 -c blue'
alias c='clear'
alias ls='ls -a'
alias etcher='sudo /home/paradox/Downloads/balenaEtcher-1.5.45-x64.AppImage'
